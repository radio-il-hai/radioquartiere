/**
 * Copyright (C) Radiojar S.A. - All Rights Reserved
 *
 * Licensed under the MIT license
 * https://opensource.org/licenses/MIT
 *
 * Created by @stathiskout
 *
 * jQuery extend-based plugin radiojar live player
 */
/*
 * This plugin initializes a radio player using the jPlayer plugin.
 * It also allows now playing information coming from Radiojar if any.
 *
 * Available options:
 * {
 *  'streamName': the mp3 stream url to play
 *  'apiKey': the api key for radio
 *  'defaultImage': default image if thumb not found
 *  'enableUpdates': gets updates regarding the song and show playing now (default:false)
 *  'updateInterval': in seconds, the time we poll for the song now playing
 */
RJP.initPlayer = function ($, $object, options) {
    //Event registration when new track is loaded to radio
    RJP.nowDataEvent = $.Event("rj-track-load-event");
    RJP.scheduleChangeEvent = $.Event("rj-schedule-change-event");
    var updateTrackDataInterval = 20000;
    RJP.nowPlayingTrack = null;
    var getTrackInterval;

    //Getting milliseconds in currentTime for unique requests
    var now = new Date();
    var currentTime = (now.getTime() - now.getMilliseconds()) / 1000;

    // Create some defaults, extending them with any options that were provided
    var settings = $.extend({}, options);

    //Building the streaming url according to radiojar format
    if (typeof settings.supplied === 'undefined') {
        settings.supplied = "mp3";
    }
    else if (settings.supplied == "aac") {
        settings.supplied = "m4a"
    }

    if (typeof settings.stream === 'undefined') {
        settings.stream = "";
    }
    else {
        settings.streamName = settings.stream;
    }

    if (typeof settings.streamUrl === 'undefined') {
        settings.streamUrl = "//stream.radiojar.com/" + settings.streamName + "." + settings.supplied;
    }

    if (typeof settings.autoplay === 'undefined') {
        settings.autoplay = true;
    }
    if (typeof settings.enableUpdates === 'undefined') {
        settings.enableUpdates = false;
    }
    var status = "starting";
    // initialize player if player exists in settings
    $player = $object;

    if (typeof settings.enableAudio === 'undefined') {
        settings.enableAudio = true;
    }

    if (settings.enableAudio) {

        $.ajax({
            url: RJP.baseUrl + "/wrappers/api-plugins/v2/jplayer/jplayer.js?",
            cache: true,
            dataType: "script",
            success: function () {

                var waitTime = 500; //time in milliseconds to wait between retries. This value changes depended on the stream status
                if ($player.jPlayer) {

                    //Destroy previous instances of jplayer for clean init
                    $player.jPlayer("destroy");

                    //jPlayer plugin initialiaztion
                    $player.jPlayer({
                        ready: function () {
                            //Event triggered when player components have been loaded, we then pass the stream to load
                            $('.jp-play').addClass('player-loading');
                            if (settings.autoplay) {
                                if (settings.supplied == "m4a") {
                                    $(this).jPlayer("setMedia", {
                                        m4a: settings.streamUrl + '?' + currentTime
                                    }).jPlayer("play");
                                }
                                else {
                                    $(this).jPlayer("setMedia", {
                                        mp3: settings.streamUrl + '?' + currentTime
                                    }).jPlayer("play");
                                }
                            }
                            $('.jp-play').removeClass('player-loading');
                            // $(".jp-volume-bar-wrapper").hide();
                            // $(".jp-mute").hide();
                            // $(".jp-unmute").hide();

                        },
                        cssSelector: {
                            volumeBar: ".jp-volume-bar",
                            volumeBarValue: ".jp-volume-bar-value"
                        },
                        pause: function () {
                            //Pause event we remove the media and then add them again in order not to cache input data
                            $(".jp-play").removeClass('player-loading');
                            $(".jp-pause").removeClass('player-loading');
                            $(".jp-play").show();
                            $(".jp-pause").hide();
                            waitTime = 500;
                            $(this).jPlayer("clearMedia");
                            if (status == "pausing") {
                                status = "paused";
                            }
                            else {
                                waitTime = 2000;
                                setTimeout(function () {
                                    if (settings.supplied == "m4a") {
                                        $player.jPlayer("setMedia", {
                                            m4a: settings.streamUrl + '?' + currentTime
                                        }).jPlayer("play");
                                    }
                                    else {
                                        $player.jPlayer("setMedia", {
                                            mp3: settings.streamUrl + '?' + currentTime
                                        }).jPlayer("play");
                                    }
                                }, waitTime);
                            }

                            var title = document.title.replace('\u25B6 ', '');
                            document.title = title;
                        },
                        play: function () {
                            $(".jp-play").removeClass('player-loading');
                            $(".jp-pause").removeClass('player-loading');
                            //Play event triggered
                            $(".jp-pause").show();
                            $(".jp-play").hide();
                            var title = document.title;
                            if (title.indexOf('\u25B6') == -1) {
                                document.title = "\u25B6 " + title;
                            }
                            status = "playing";
                        },
                        stop: function () {
                            //Stop event triggered
                            status = "paused";
                            $(this).jPlayer("clearMedia");
                        },
                        volumechange: function (event) {
                            //Volume event triggered, mute & unmute
                        },
                        error: function (event) {
                            // Error event triggered, we clear the media and then add it again

                            $(this).jPlayer("clearMedia");
                            switch (event.jPlayer.error.type) {
                                case $.jPlayer.error.NO_SOLUTION:
                                    $(".jp-no-solution").show();
                                    break;
								case $.jPlayer.error.URL:
									if (detectIE()) {
										$('.jp-play').removeClass('player-loading');
										alert("The specific file format is not supported by Internet Expoler.\n" +
											"Please use other browser.");
										break;
									}
                                default:
                                    if ($('.jp-play').is(':visible')) {
                                        $('.jp-play').addClass('player-loading');
                                    }
                                    if ($('.jp-pause').is(':visible')) {
                                        $('.jp-pause').addClass('player-loading');
                                    }
                                    currentTime = new Date();
                                    waitTime = 3000;
                                    setTimeout(function () {
                                        if (settings.supplied == "m4a") {
                                            $player.jPlayer("setMedia", {
                                                m4a: settings.streamUrl + '?' + currentTime
                                            }).jPlayer("play");
                                        }
                                        else {
                                            $player.jPlayer("setMedia", {
                                                mp3: settings.streamUrl + '?' + currentTime
                                            }).jPlayer("play");
                                        }
                                    }, waitTime);
                                    break;
                            }

                        },
                        ended: function (event) {
                            //End event triggered i.e. connection lost
                            $(this).jPlayer("clearMedia");
                            currentTime = new Date();
                            waitTime = 3000;
                            setTimeout(function () {
                                if (settings.supplied == "m4a") {
                                    $player.jPlayer("setMedia", {
                                        m4a: settings.streamUrl + '?' + currentTime
                                    }).jPlayer("play");
                                }
                                else {
                                    $player.jPlayer("setMedia", {
                                        mp3: settings.streamUrl + '?' + currentTime
                                    }).jPlayer("play");
                                }
                            }, waitTime);
                        },
                        swfPath: RJP.baseUrl + "/wrappers/api-plugins/v2/jplayer/",

                        solution: "html, flash",
                        supplied: settings.supplied

                    });
                }
                else {
                    // jPlayer is not loaded
                }
                var stop = function () {
                    $player.jPlayer('stop');
                };
                var pause = function () {
                    $player.jPlayer('pause');
                };
                //initializing buttons for jPlayer
                $(".jp-play").unbind('click');
                $(".jp-play").click(function (ev) {
                    ev.preventDefault();
                    $(this).addClass('player-loading');
                    $('.jp-pause').hide();
                    $(this).show();
                    if (settings.supplied == "m4a") {
                        $player.jPlayer("setMedia", {
                            m4a: settings.streamUrl + '?' + currentTime
                        });
                    }
                    else {
                        $player.jPlayer("setMedia", {
                            mp3: settings.streamUrl + '?' + currentTime
                        });
                    }
                    $player.jPlayer('play');
                });
                $(".jp-pause").unbind('click');
                $(".jp-pause").click(function (ev) {
                    status = "pausing";
                    ev.preventDefault();
                    $(this).addClass('player-loading');
                    $('.jp-play').hide();
                    $(this).show();
                    $player.jPlayer('pause');
                });
                $(".jp-mute").unbind('click');
                $(".jp-mute").click(function (ev) {
                    ev.preventDefault();
                    $player.jPlayer('mute');
                    $(".jp-mute").hide();
                    $(".jp-unmute").show();
                });
                $(".jp-unmute").unbind('click');
                $(".jp-unmute").click(function (ev) {
                    ev.preventDefault();
                    $player.jPlayer('unmute');
                    $(".jp-mute").show();
                    $(".jp-unmute").hide();
                });
                $(".jp-volume-bar").unbind('click');
                $(".jp-volume-bar").click(function (ev) {
                    ev.preventDefault();
                    var offset = 0;
                    var position = $(ev.currentTarget).offset();
                    if ($(ev.currentTarget).hasClass('topBar-volume') || !$(ev.currentTarget)
                            .hasClass('jp-volume-popup')) {
                        offset = position.left;
                        var volumeBarWidth = $(ev.currentTarget).width();
                        var positionClicked = (ev.clientX - offset) * 100 / volumeBarWidth;
                        $('.jp-volume-bar-value').attr('style', 'width:' + positionClicked + '%');
                        var volumeValue = (ev.clientX - offset) / volumeBarWidth;
                        $player.jPlayer('volume', volumeValue);
                    }
                    else {
                        offset = 111;
                        var positionClicked = ev.clientX - offset;
                        $('.jp-volume-bar-value').attr('style', 'width:' + positionClicked + '%');
                        var volumeValue = (ev.clientX - offset) / 100;
                        $player.jPlayer('volume', volumeValue);
                    }
                });
            }
        });

    }

    if (!settings.enableUpdates) {
        return;
    }

    // ====================== GET TRACK DATA ==============================
    var xhr;
    var now_playing_url = "//proxy.radiojar.com";

    function getTrackData() {
        if (xhr && xhr.readyState != 4) {
            xhr.abort();
        }
        xhr =
            $.ajax({
                type: 'GET',
                crossDomain: true,
                cache: false,
                url: now_playing_url + "/api/stations/" + settings.streamName + "/now_playing/?callback=?",
                dataType: 'jsonp',
                async: true,
                success: function (data) {
                    if (data) {
                        if (RJP.nowPlayingTrack == null || RJP.nowPlayingTrack.title != data.title) {
                            RJP.nowPlayingTrack = data;
                            if (!RJP.nowPlayingTrack.thumb) {
                                RJP.nowPlayingTrack.thumb = settings.defaultImage;
                            }
                            else {
                                RJP.nowPlayingTrack.thumb = updateQueryStringParameter(RJP.nowPlayingTrack.thumb, "size", "custom");
                                RJP.nowPlayingTrack.thumb = updateQueryStringParameter(RJP.nowPlayingTrack.thumb, "w", "150");
                                RJP.nowPlayingTrack.thumb = updateQueryStringParameter(RJP.nowPlayingTrack.thumb, "h", "150");
                            }
                            RJP.nowDataEvent.data = RJP.nowPlayingTrack;
                            $object.trigger(RJP.nowDataEvent, RJP.nowPlayingTrack);
                        }
                    }
                },
                error: function (xhr, status, errorThrown) {
                }
            });
    }

    function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }

    $.fn.stopTrackLoading = function () {
        clearInterval(getTrackInterval);
    };
    $($object).on("remove", function () {
        clearInterval(getTrackInterval);
    });

    if (settings.streamName) {
        getTrackData();
        clearInterval(getTrackInterval);
        getTrackInterval = setInterval(function () {
            getTrackData();
        }, updateTrackDataInterval);
    }
    // ====================== GET TRACK DATA ==============================

    return $object.each(function () {
    });
};

function musesCallback(event, param) {
    switch (event) {
        case "buffering":
            rjq('.jp-play').addClass('player-loading');
            rjq('.jp-pause').addClass('player-loading');
            break;
        case "play":
            rjq(".jp-play").removeClass('player-loading');
            rjq(".jp-pause").removeClass('player-loading');
            //Play event triggered
            rjq(".jp-pause").show();
            rjq(".jp-play").hide();
            var title = document.title;
            if (title.indexOf('\u25B6') == -1) {
                document.title = "\u25B6 " + title;
            }
            status = "playing";
            break;
        case "stop":
            //Pause event we remove the media and then add them again in order not to cache input data
            rjq(".jp-play").removeClass('player-loading');
            rjq(".jp-pause").removeClass('player-loading');
            rjq(".jp-play").show();
            rjq(".jp-pause").hide();

            var title = document.title.replace('\u25B6 ', '');
            document.title = title;
        case "ioError":
        case "intro":
            rjq('.jp-play').removeClass('player-loading');
            rjq('.jp-pause').removeClass('player-loading');
            break;
        case "metadata-json":
            break;
        case "metadata":
            break;
        case "source":
            flashPlayer.setFallbackUrl(settings.streamUrl);
            break;
        case "fallback":
            flashPlayer.setFallbackUrl(settings.streamUrl);
            break;
        case "intro-url":
            flashPlayer.setFallbackUrl(settings.streamUrl);
            break;
        default:
            break;
    }
}

function detectIE() {
	var ua = window.navigator.userAgent;
	if (ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident/') > 0) {
		// IE 10 or older or IE 11
		return true;
	}
	// other browser
	return false;
}
